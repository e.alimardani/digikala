import Empty from 'components/shared/Empty';
import IconApp from 'components/shared/IconApp';
import TextApp from 'components/shared/TextApp';
import {colors} from 'global/colors';
import {fonts} from 'global/fonts';
import React, {useEffect, useState} from 'react';
import {FlatList, StyleSheet, TouchableOpacity, View} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import {setCategory, setMovie} from 'redux/Actions';
import {
  REMOVE_CATEGORY_SELECTION,
  SET_CATEGORY_SELECTION,
  REMOVE_CATEGORY_SELECTION_ALL,
  SET_MOVIES,
} from 'redux/Actions/Type';
import api from 'service/apis';
import {fontSize, normalize} from 'utilities/fontSize';

const CategoryContent = () => {
  const dispatch = useDispatch();

  const categories = useSelector(state => state.categories);

  const [isLoading, setIsLoading] = useState(true);

  const selectCategory = item => {
    let isSelected = categories.categorySelected.find(dataItem => {
      return dataItem.name === item.name;
    });
    if (!isSelected) {
      dispatch(setCategory({type: SET_CATEGORY_SELECTION, payload: item}));
    } else {
      dispatch(setCategory({type: REMOVE_CATEGORY_SELECTION, payload: item}));
    }
  };

  const clearFilter = () => {
    dispatch(setCategory({type: REMOVE_CATEGORY_SELECTION_ALL}));
  };

  useEffect(() => {
    let filterData = '';
    categories.categorySelected.map((item, index) => {
      filterData = filterData.concat(`&genre[${index}]=${item.name}`);
    });
    api()
      .getMovies(categories.title, filterData)
      .then(res => {
        dispatch(setMovie({type: SET_MOVIES, payload: res.data.movies}));
        setIsLoading(false);
      })
      .catch(() => {
        setIsLoading(false);
      });
  }, [categories, dispatch]);

  const renderItem = ({item}) => {
    let isSelected = categories.categorySelected.find(dataItem => {
      return dataItem.name === item.name;
    });
    return (
      <TouchableOpacity
        onPress={() => selectCategory(item)}
        activeOpacity={0.7}
        style={styles.item}>
        <TextApp
          style={styles.itemTitle}
          font={fonts.medium}
          size={fontSize.xlarge}>
          {item.name}
        </TextApp>
        {isSelected && (
          <IconApp
            color={colors.dark}
            size={normalize(20)}
            name="checkmark-circle"
          />
        )}
      </TouchableOpacity>
    );
  };
  return (
    <>
      <View style={styles.header}>
        <TextApp
          style={styles.titleCategory}
          font={fonts.bold}
          size={fontSize.xxlarge}>
          Category List
        </TextApp>
        <TouchableOpacity
          onPress={clearFilter}
          activeOpacity={0.7}
          style={styles.clear}>
          <TextApp
            style={styles.clearText}
            font={fonts.regular}
            size={fontSize.medium}>
            clear all
          </TextApp>
          <IconApp color={colors.dark} size={normalize(18)} name="trash" />
        </TouchableOpacity>
      </View>
      <FlatList
        data={categories.data}
        keyExtractor={item => item.id.toString()}
        renderItem={renderItem}
        ListEmptyComponent={!isLoading && <Empty />}
        showsVerticalScrollIndicator={false}
      />
    </>
  );
};

export default CategoryContent;

const styles = StyleSheet.create({
  item: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingVertical: normalize(10),
    marginBottom: normalize(10),
    borderBottomColor: colors.dark,
    borderBottomWidth: 0.6,
  },
  itemTitle: {
    color: colors.dark,
  },
  titleCategory: {
    color: colors.dark,
  },
  header: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginBottom: normalize(20),
  },
  clear: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  clearText: {
    color: colors.dark,
    marginRight: normalize(5),
  },
});
