import {colors} from 'global/colors';
import {fonts} from 'global/fonts';
import React from 'react';
import {ActivityIndicator, StyleSheet, TouchableOpacity} from 'react-native';
import {fontSize} from 'utilities/fontSize';
import TextApp from './TextApp';

const ButtonApp = props => {
  const {title, isLoading} = props;
  return (
    <TouchableOpacity
      {...props}
      disabled={isLoading}
      activeOpacity={0.8}
      style={[styles.btn, isLoading && styles.deactive]}>
      {isLoading ? (
        <ActivityIndicator size="large" color={colors.dark} />
      ) : (
        <TextApp
          font={fonts.bold}
          size={fontSize.large}
          style={styles.btnTitle}>
          {title}
        </TextApp>
      )}
    </TouchableOpacity>
  );
};

export default ButtonApp;

const styles = StyleSheet.create({
  btn: {
    flex: 1,
    backgroundColor: colors.yellow,
    height: 45,
    borderRadius: 5,
    alignItems: 'center',
    justifyContent: 'center',
  },
  btnTitle: {
    color: colors.dark,
  },
  deactive: {
    opacity: 0.8,
  },
});
