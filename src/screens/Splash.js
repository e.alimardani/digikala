import AsyncStorage from '@react-native-async-storage/async-storage';
import TextApp from 'components/shared/TextApp';
import {colors} from 'global/colors';
import {fonts} from 'global/fonts';
import React, {useEffect} from 'react';
import {View, StyleSheet, ActivityIndicator} from 'react-native';
import {Actions} from 'react-native-router-flux';
import {useDispatch} from 'react-redux';
import {setCategory} from 'redux/Actions';
import {SET_CATEGORY_LIST} from 'redux/Actions/Type';
import api from 'service/apis';
import {fontSize, normalize} from 'utilities/fontSize';

const Splash = () => {
  const dispatch = useDispatch();

  useEffect(() => {
    const checkLogin = async () => {
      const TOKEN = await AsyncStorage.getItem('token');

      if (TOKEN) {
        getCategory();
      } else {
        Actions.reset('login');
      }
    };

    const getCategory = () => {
      api()
        .getCategories()
        .then(res => {
          // for remove similar items
          let result = res.data.genres.filter(
            (v, i, a) => a.findIndex(t => t.name === v.name) === i,
          );
          dispatch(setCategory({type: SET_CATEGORY_LIST, payload: result}));
          Actions.reset('home');
        })
        .catch(() => {
          return false;
        });
    };
    checkLogin();
  }, [dispatch]);

  return (
    <View style={styles.container}>
      <View>
        <ActivityIndicator color={colors.white} size="large" />
        <TextApp
          style={styles.title}
          font={fonts.bold}
          size={fontSize.xxxlarge}>
          Please Wait ...
        </TextApp>
      </View>
    </View>
  );
};

export default Splash;

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.dark,
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  title: {
    marginTop: normalize(5),
  },
});
