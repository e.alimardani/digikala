import IconApp from 'components/shared/IconApp';
import InputApp from 'components/shared/InputApp';
import React, {useState} from 'react';
import {View, StyleSheet} from 'react-native';
import {normalize} from 'utilities/fontSize';

const SearchBar = props => {
  const {onSearch} = props;

  const [input, setInput] = useState('');

  return (
    <View style={styles.container}>
      <InputApp
        keyboardType="web-search"
        style={styles.input}
        placeholder="Search"
        returnKeyType="search"
        value={input}
        onChangeText={setInput}
        onSubmitEditing={() => onSearch(input)}
      />
      <IconApp
        onPress={() => onSearch(input)}
        size={normalize(30)}
        style={styles.icon}
        name="search"
      />
    </View>
  );
};

export default SearchBar;

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    marginVertical: normalize(10),
    marginHorizontal: normalize(25),
  },
  input: {
    flex: 1,
  },
  icon: {
    marginLeft: normalize(10),
  },
});
