import IconApp from 'components/shared/IconApp';
import ModalApp from 'components/shared/ModalApp';
import TextApp from 'components/shared/TextApp';
import {colors} from 'global/colors';
import {fonts} from 'global/fonts';
import React, {useState} from 'react';
import {View, StyleSheet, TouchableOpacity} from 'react-native';
import {useSelector} from 'react-redux';
import {fontSize, normalize} from 'utilities/fontSize';
import CategoryContent from './CategoryContent';

const Category = () => {
  const categorySelected = useSelector(
    state => state.categories.categorySelected,
  );
  const movies = useSelector(state => state.movies.data);

  const [isModal, setIsModal] = useState(false);
  let isSelected = categorySelected.length > 0;
  return (
    <View style={styles.container}>
      <TextApp font={fonts.bold} size={fontSize.xxlarge}>
        {movies.length} Movies
      </TextApp>
      <TouchableOpacity
        onPress={() => setIsModal(true)}
        activeOpacity={0.8}
        style={styles.categories}>
        <IconApp
          color={isSelected ? colors.yellow : colors.white}
          size={normalize(24)}
          name="options-outline"
        />
        <TextApp
          style={[styles.title, isSelected && styles.countText]}
          font={fonts.regular}
          size={fontSize.large}>
          Category
        </TextApp>
        {isSelected && (
          <View style={styles.count}>
            <TextApp
              font={fonts.medium}
              size={fontSize.small}
              style={styles.countText}>
              {categorySelected.length}
            </TextApp>
          </View>
        )}
      </TouchableOpacity>
      <ModalApp isVisible={isModal} onClose={() => setIsModal(false)}>
        <CategoryContent />
      </ModalApp>
    </View>
  );
};

export default Category;

const styles = StyleSheet.create({
  categories: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: normalize(10),
    marginTop: normalize(5),
    justifyContent: 'space-between',
    marginHorizontal: normalize(25),
  },
  title: {
    marginLeft: normalize(5),
  },
  count: {
    borderWidth: 1.5,
    borderColor: colors.yellow,
    height: normalize(25),
    width: normalize(25),
    borderRadius: normalize(15),
    alignItems: 'center',
    justifyContent: 'center',
    marginLeft: normalize(7),
  },
  countText: {
    color: colors.yellow,
  },
});
