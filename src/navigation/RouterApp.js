import {Scene, Stack, Router} from 'react-native-router-flux';
import React from 'react';
import Login from 'screens/Login';
import BackPress from 'utilities/backPress';
import Home from 'screens/Home';
import Splash from 'screens/Splash';
import {Provider} from 'react-redux';
import store from 'redux/Store';

const RouterApp = () => {
  return (
    <Provider store={store}>
      <Router backAndroidHandler={BackPress}>
        <Stack hideNavBar>
          <Scene key="splash" component={Splash} />
          <Scene key="login" component={Login} />
          <Scene key="home" component={Home} />
        </Stack>
      </Router>
    </Provider>
  );
};
export default RouterApp;
