export const colors = {
  dark: '#000',
  white: '#fff',
  gray: '#808080',
  yellow: '#f5c518',
  red: '#f33',
  green: '#32cd32',
};
