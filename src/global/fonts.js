export const fonts = {
  bold: 'Roboto-Bold',
  medium: 'Roboto-Medium',
  regular: 'Roboto-Regular',
  light: 'Roboto-Light',
  thin: 'Roboto-Thin',
};
