import React from 'react';
import RouterApp from 'navigation/RouterApp';
import Layout from 'components/generic/Layout';

const App = () => {
  return (
    <Layout>
      <RouterApp />
    </Layout>
  );
};

export default App;
