import TextApp from 'components/shared/TextApp';
import {colors} from 'global/colors';
import {fonts} from 'global/fonts';
import React from 'react';
import {StyleSheet} from 'react-native';
import {fontSize, normalize} from 'utilities/fontSize';

const Option = props => {
  const {title, content} = props;
  return (
    <TextApp font={fonts.regular} size={fontSize.small} style={styles.title}>
      {title}: <TextApp style={styles.content}>{content}</TextApp>
    </TextApp>
  );
};

export default Option;

const styles = StyleSheet.create({
  title: {
    color: colors.gray,
    marginTop: normalize(5),
  },
  content: {},
});
