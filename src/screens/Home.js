import Category from 'components/home/Category';
import SearchBar from 'components/home/SearchBar';
import SingleMovie from 'components/home/SingleMovie';
import Empty from 'components/shared/Empty';
import HeaderApp from 'components/shared/HeaderApp';
import {colors} from 'global/colors';
import React, {useEffect, useState} from 'react';
import {View, StyleSheet, FlatList} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import {setCategory, setMovie} from 'redux/Actions';
import {SET_MOVIES, SET_TITLE_MOVIE} from 'redux/Actions/Type';
import api from 'service/apis';
import {normalize} from 'utilities/fontSize';

const Home = () => {
  const dispatch = useDispatch();
  const categories = useSelector(state => state.categories);

  const [isLoading, setIsLoading] = useState(true);
  const movies = useSelector(state => state.movies.data);

  const getMovies = (inp = '') => {
    setIsLoading(true);

    let filterData = '';
    categories.categorySelected.map((item, index) => {
      filterData = filterData.concat(`&genre[${index}]=${item.name}`);
    });

    dispatch(setCategory({type: SET_TITLE_MOVIE, payload: inp}));

    api()
      .getMovies(inp, filterData)
      .then(res => {
        dispatch(setMovie({type: SET_MOVIES, payload: res.data.movies}));
        setIsLoading(false);
      })
      .catch(() => {
        setIsLoading(false);
      });
  };

  useEffect(() => {
    setIsLoading(true);
    api()
      .getMovies('', '')
      .then(res => {
        dispatch(setMovie({type: SET_MOVIES, payload: res.data.movies}));
        setIsLoading(false);
      })
      .catch(() => {
        setIsLoading(false);
      });
  }, [dispatch]);

  return (
    <View style={styles.container}>
      <HeaderApp title="Home" isLogOut />
      <SearchBar onSearch={text => getMovies(text)} />
      <Category />
      <FlatList
        data={movies}
        keyExtractor={item => item.title}
        renderItem={SingleMovie}
        refreshing={isLoading}
        onRefresh={getMovies}
        contentContainerStyle={styles.flatList}
        ListEmptyComponent={!isLoading && <Empty />}
      />
    </View>
  );
};

export default Home;

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.dark,
    flex: 1,
  },
  flatList: {
    padding: normalize(25),
  },
  searchBar: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  input: {
    flex: 1,
  },
});
