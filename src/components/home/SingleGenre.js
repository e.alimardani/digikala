import TextApp from 'components/shared/TextApp';
import {colors} from 'global/colors';
import {fonts} from 'global/fonts';
import React from 'react';
import {StyleSheet, View} from 'react-native';
import {fontSize, normalize} from 'utilities/fontSize';

const SingleGenre = props => {
  const {data} = props;
  return (
    <View style={styles.genres}>
      <TextApp
        font={fonts.regular}
        size={fontSize.small}
        style={styles.genreTitle}>
        Genre:{' '}
      </TextApp>
      {data.map((item, index) => {
        return (
          <TextApp
            key={item.toString()}
            font={fonts.regular}
            size={fontSize.small}>
            {' '}
            {item}
            {index + 1 !== data.length ? ',' : ''}
          </TextApp>
        );
      })}
    </View>
  );
};

export default SingleGenre;

const styles = StyleSheet.create({
  genres: {
    flexDirection: 'row',
    marginTop: normalize(5),
    flex: 1,
    flexWrap: 'wrap',
  },
  genreTitle: {
    color: colors.gray,
  },
});
