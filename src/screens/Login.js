import AsyncStorage from '@react-native-async-storage/async-storage';
import ButtonApp from 'components/shared/ButtonApp';
import HeaderApp from 'components/shared/HeaderApp';
import InputApp from 'components/shared/InputApp';
import TextApp from 'components/shared/TextApp';
import {Formik} from 'formik';
import {colors} from 'global/colors';
import {fonts} from 'global/fonts';
import React, {useRef, useState} from 'react';
import {
  View,
  StyleSheet,
  KeyboardAvoidingView,
  ScrollView,
  Keyboard,
} from 'react-native';
import {Actions} from 'react-native-router-flux';
import {useDispatch} from 'react-redux';
import {setCategory} from 'redux/Actions';
import {SET_CATEGORY_LIST} from 'redux/Actions/Type';
import api from 'service/apis';
import {fontSize, normalize} from 'utilities/fontSize';
import {isEmail} from 'utilities/helper';
import * as Yup from 'yup';

Yup.addMethod(Yup.string, 'isEmail', function (errorMessage) {
  return this.test('is-mobile', errorMessage, function (value) {
    const {path, createError} = this;
    return isEmail(value) || createError({path, message: errorMessage});
  });
});

const SignInSchema = Yup.object().shape({
  username: Yup.string()
    .min(11, 'username too short !')
    .required('username required !')
    .isEmail('invalid username !'),
  password: Yup.string()
    .min(8, 'password too short !')
    .required('password required !'),
});

const Login = () => {
  const dispatch = useDispatch();

  const inputRef = useRef(null);

  const [isLoading, setIsLoading] = useState(false);

  const onLogin = values => {
    Keyboard.dismiss();
    setIsLoading(true);

    let data = {
      username: values.username,
      password: values.password,
    };
    api()
      .login(data)
      .then(res => {
        AsyncStorage.setItem('token', res.data.token);
        getCategory();
      })
      .catch(() => {
        setIsLoading(false);
      });
  };

  const getCategory = () => {
    api()
      .getCategories()
      .then(res => {
        // for remove similar items
        let result = res.data.genres.filter(
          (v, i, a) => a.findIndex(t => t.name === v.name) === i,
        );
        dispatch(setCategory({type: SET_CATEGORY_LIST, payload: result}));
        setIsLoading(false);
        Actions.reset('home');
      })
      .catch(() => {
        setIsLoading(false);
      });
  };

  return (
    <View style={styles.container}>
      <HeaderApp title="Login" />
      <Formik
        initialValues={{
          username: '',
          password: '',
        }}
        onSubmit={onLogin}
        validationSchema={SignInSchema}>
        {({handleChange, handleSubmit, values, errors, touched}) => {
          return (
            <ScrollView
              keyboardShouldPersistTaps="handled"
              style={styles.container}
              contentContainerStyle={styles.content}>
              <KeyboardAvoidingView enabled behavior="padding">
                <TextApp
                  style={styles.title}
                  font={fonts.medium}
                  size={fontSize.medium}>
                  Please Enter Your Username and Password
                </TextApp>
                <InputApp
                  onSubmitEditing={() => inputRef.current.focus()}
                  keyboardType="email-address"
                  placeholder="Username"
                  returnKeyType="next"
                  value={values.username}
                  onChangeText={handleChange('username')}
                  isError={errors.username && touched.username}
                  errorMsg={errors.username}
                  style={styles.input}
                />
                <InputApp
                  secureTextEntry
                  placeholder="Password"
                  returnKeyType="done"
                  ref={inputRef}
                  value={values.password}
                  onChangeText={handleChange('password')}
                  isError={errors.password && touched.password}
                  errorMsg={errors.password}
                  style={styles.input}
                />
                <ButtonApp
                  isLoading={isLoading}
                  onPress={handleSubmit}
                  title="Sign In"
                />
              </KeyboardAvoidingView>
            </ScrollView>
          );
        }}
      </Formik>
    </View>
  );
};

export default Login;

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.dark,
    flex: 1,
  },
  title: {
    marginBottom: normalize(25),
  },
  content: {
    padding: normalize(25),
    justifyContent: 'center',
    flexGrow: 1,
  },
  input: {
    marginBottom: normalize(15),
  },
});
