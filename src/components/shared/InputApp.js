import {colors} from 'global/colors';
import {fonts} from 'global/fonts';
import React, {forwardRef, useEffect, useState} from 'react';
import {TextInput, StyleSheet, LayoutAnimation, View} from 'react-native';
import {fontSize, normalize} from 'utilities/fontSize';
import {customAnim} from 'utilities/helper';
import TextApp from './TextApp';

const InputApp = forwardRef((props, ref) => {
  const {isError, errorMsg, style} = props;
  const [isFocus, setIsFocus] = useState(false);
  const [hasError, setHasError] = useState(false);

  useEffect(() => {
    LayoutAnimation.configureNext(customAnim(150));
    setHasError(isError);
  }, [isError]);

  return (
    <View style={style}>
      <TextInput
        {...props}
        onFocus={() => setIsFocus(true)}
        onBlur={() => setIsFocus(false)}
        placeholderTextColor={colors.gray}
        underlineColorAndroid={colors.dark}
        selectionColor={colors.white}
        ref={ref}
        style={[
          styles.input,
          isFocus && styles.focus,
          hasError && styles.errorInput,
        ]}
      />
      {hasError && (
        <TextApp
          style={styles.error}
          font={fonts.regular}
          size={fontSize.small}>
          {errorMsg}
        </TextApp>
      )}
    </View>
  );
});

export default InputApp;

const styles = StyleSheet.create({
  input: {
    fontSize: fontSize.medium,
    fontFamily: fonts.medium,
    borderColor: colors.white,
    borderWidth: 1.5,
    paddingHorizontal: normalize(10),
    paddingVertical: normalize(7),
    color: colors.white,
    borderRadius: 5,
  },
  focus: {
    borderColor: colors.yellow,
  },
  errorInput: {
    borderColor: colors.red,
  },
  error: {
    color: colors.red,
  },
});
