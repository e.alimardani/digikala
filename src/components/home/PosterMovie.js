import Empty from 'components/shared/Empty';
import React from 'react';
import {Image, StyleSheet} from 'react-native';
import {normalize} from 'utilities/fontSize';

const PosterMovie = props => {
  const {poster} = props;
  return poster !== 'N/A' ? (
    <Image
      resizeMode="stretch"
      style={styles.img}
      source={{uri: poster.replace(' =>', ':')}}
    />
  ) : (
    <Empty />
  );
};

export default PosterMovie;

const styles = StyleSheet.create({
  img: {
    flex: 1,
    height: normalize(130),
    marginRight: normalize(10),
  },
});
