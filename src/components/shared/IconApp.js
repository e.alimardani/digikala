import {colors} from 'global/colors';
import React from 'react';
import Icon from 'react-native-vector-icons/Ionicons';

const IconApp = props => <Icon color={colors.white} {...props} />;

export default IconApp;
