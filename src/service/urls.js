export const urls = {
  LOGIN: 'login/',
  MOVIE: 'movies/',
  CATEGORY: 'genres/?rows=20',
};
