import {combineReducers} from 'redux';
import movies from './movieReducer';
import categories from './categoryReducer';

export default combineReducers({
  movies,
  categories,
});
