import {
  REMOVE_CATEGORY_SELECTION,
  REMOVE_CATEGORY_SELECTION_ALL,
  SET_CATEGORY_LIST,
  SET_CATEGORY_SELECTION,
  SET_TITLE_MOVIE,
} from 'redux/Actions/Type';

const initialState = {
  data: [],
  title: '',
  categorySelected: [],
};

const categoryReducer = (state = initialState, action = {}) => {
  const {type, payload} = action;
  switch (type) {
    case SET_CATEGORY_LIST:
      return {
        ...state,
        data: payload,
      };
    case SET_CATEGORY_SELECTION:
      return {
        ...state,
        categorySelected: state.categorySelected.concat(payload),
      };

    case REMOVE_CATEGORY_SELECTION:
      return {
        ...state,
        categorySelected: state.categorySelected.filter(item => {
          return item.name !== payload.name;
        }),
      };

    case REMOVE_CATEGORY_SELECTION_ALL:
      return {
        ...state,
        categorySelected: [],
      };

    case SET_TITLE_MOVIE:
      return {
        ...state,
        title: payload,
      };
    default:
      return state;
  }
};
export default categoryReducer;
