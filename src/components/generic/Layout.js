import ToastApp from 'components/shared/ToastApp';
import {colors} from 'global/colors';
import React from 'react';
import {
  Platform,
  SafeAreaView,
  StatusBar,
  StyleSheet,
  UIManager,
} from 'react-native';
import {GestureHandlerRootView} from 'react-native-gesture-handler';

if (
  Platform.OS === 'android' &&
  UIManager.setLayoutAnimationEnabledExperimental
) {
  UIManager.setLayoutAnimationEnabledExperimental(true);
}

const Layout = props => {
  return (
    <SafeAreaView style={styles.container}>
      <StatusBar
        animated
        barStyle="light-content"
        backgroundColor={colors.dark}
      />
      <GestureHandlerRootView style={styles.content}>
        {props.children}
        <ToastApp />
      </GestureHandlerRootView>
    </SafeAreaView>
  );
};
export default Layout;
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.dark,
  },
  content: {
    flex: 1,
    backgroundColor: colors.dark,
  },
});
