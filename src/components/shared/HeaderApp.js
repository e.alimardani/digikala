import AsyncStorage from '@react-native-async-storage/async-storage';
import {fonts} from 'global/fonts';
import React from 'react';
import {View, StyleSheet} from 'react-native';
import {Actions} from 'react-native-router-flux';
import {fontSize, normalize} from 'utilities/fontSize';
import IconApp from './IconApp';
import TextApp from './TextApp';

const HeaderApp = props => {
  const {title, isBack, isLogOut} = props;

  const onLogout = async () => {
    await AsyncStorage.removeItem('token');
    Actions.reset('login');
  };
  return (
    <View style={styles.container}>
      {isBack && (
        <IconApp
          onPress={() => Actions.pop()}
          style={styles.iconBack}
          size={fontSize.giant}
          name="arrow-back-outline"
        />
      )}
      <TextApp style={styles.title} font={fonts.bold} size={fontSize.giant}>
        {title}
      </TextApp>
      {isLogOut && (
        <IconApp
          onPress={onLogout}
          style={styles.iconLogOut}
          size={fontSize.giant}
          name="log-out-outline"
        />
      )}
    </View>
  );
};

export default HeaderApp;

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: normalize(10),
    paddingHorizontal: normalize(20),
  },
  title: {
    flex: 1,
    textAlign: 'center',
  },
  iconBack: {
    position: 'absolute',
    left: normalize(20),
  },
  iconLogOut: {
    position: 'absolute',
    right: normalize(20),
  },
});
