export const setMovie = ({type, payload}) => ({
  type,
  payload,
});

export const setCategory = ({type, payload}) => ({
  type,
  payload,
});
