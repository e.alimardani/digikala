import {fontSize, normalize} from 'utilities/fontSize';
import Toast from 'react-native-toast-message';
import {View, StyleSheet} from 'react-native';
import {shadows} from 'global/shadows';
import {colors} from 'global/colors';
import TextApp from './TextApp';
import IconApp from './IconApp';
import React from 'react';
import {fonts} from 'global/fonts';

const toastConfig = {
  success: ({text1}) => (
    <View style={styles.container}>
      <View style={styles.content}>
        <View style={[styles.WrapperIcon, styles.successIcon]}>
          <IconApp
            color={colors.white}
            name="checkmark-circle-outline"
            size={normalize(26)}
          />
        </View>
        <TextApp
          font={fonts.regular}
          size={fontSize.small}
          style={styles.title}>
          {text1}
        </TextApp>
      </View>
    </View>
  ),
  error: ({text1}) => (
    <View style={styles.container}>
      <View style={styles.content}>
        <View style={[styles.WrapperIcon, styles.errorIcon]}>
          <IconApp
            color={colors.white}
            name="close-circle-outline"
            size={normalize(26)}
          />
        </View>
        <TextApp
          font={fonts.regular}
          size={fontSize.small}
          style={styles.title}>
          {text1}
        </TextApp>
      </View>
    </View>
  ),
  warning: ({text1}) => (
    <View style={styles.container}>
      <View style={styles.content}>
        <View style={[styles.WrapperIcon, styles.warningIcon]}>
          <IconApp
            color={colors.white}
            name="warning-outline"
            size={normalize(26)}
          />
        </View>
        <TextApp
          font={fonts.regular}
          size={fontSize.small}
          style={styles.title}>
          {text1}
        </TextApp>
      </View>
    </View>
  ),
};

const ToastApp = () => {
  return (
    <Toast
      visibilityTime={2000}
      topOffset={20}
      config={toastConfig}
      ref={ref => Toast.setRef(ref)}
    />
  );
};

export default ToastApp;

const styles = StyleSheet.create({
  container: {
    height: 68,
    width: '100%',
    backgroundColor: 'transparent',
  },
  content: {
    backgroundColor: colors.white,
    marginHorizontal: normalize(20),
    ...shadows.ShadowSix,
    borderRadius: 8,
    flexDirection: 'row',
    alignItems: 'center',
  },
  WrapperIcon: {
    width: normalize(56),
    height: normalize(60),
    borderTopLeftRadius: 8,
    borderBottomLeftRadius: 8,
    marginRight: normalize(10),
    alignItems: 'center',
    justifyContent: 'center',
  },
  successIcon: {
    backgroundColor: colors.green,
  },
  errorIcon: {
    backgroundColor: colors.red,
  },
  warningIcon: {
    backgroundColor: colors.yellow,
  },
  title: {
    color: colors.dark,
    flex: 1,
  },
});
