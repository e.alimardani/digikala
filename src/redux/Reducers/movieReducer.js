import {SET_MOVIES} from 'redux/Actions/Type';

const initialState = {
  data: [],
};

const movieReducer = (state = initialState, action = {}) => {
  const {type, payload} = action;
  switch (type) {
    case SET_MOVIES:
      return {
        ...state,
        data: payload,
      };
    default:
      return state;
  }
};
export default movieReducer;
