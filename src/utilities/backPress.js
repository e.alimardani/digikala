import {Actions} from 'react-native-router-flux';
import {BackHandler, ToastAndroid} from 'react-native';

let backButtonPressedOnceToExit = false;

const BackPress = () => {
  if (backButtonPressedOnceToExit) {
    BackHandler.exitApp();
  } else {
    if (Actions.currentScene === 'home' || 'login') {
      backButtonPressedOnceToExit = true;
      ToastAndroid.show(
        'To exit, press the back button again',
        ToastAndroid.LONG,
      );
      setTimeout(() => {
        backButtonPressedOnceToExit = false;
      }, 2000);
      return true;
    } else {
      Actions.pop();
      return true;
    }
  }
};

export default BackPress;
