import {Dimensions, PixelRatio} from 'react-native';

const {width: SCREEN_WIDTH, height: SCREEN_HEIGHT} = Dimensions.get('window');

export const normalize = size => {
  const localWidth =
    SCREEN_WIDTH < SCREEN_HEIGHT ? SCREEN_WIDTH : SCREEN_HEIGHT;
  const scale = localWidth / 480;
  const newSize = size * scale;
  return Math.round(PixelRatio.roundToNearestPixel(newSize));
};

export const fontSize = {
  mini: normalize(12),
  small: normalize(14),
  medium: normalize(16),
  large: normalize(18),
  xlarge: normalize(20),
  xxlarge: normalize(22),
  xxxlarge: normalize(24),
  huge: normalize(28),
  giant: normalize(32),
  ultra: normalize(36),
  xultra: normalize(38),
  xxultra: normalize(40),
};
