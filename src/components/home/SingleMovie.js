import IconApp from 'components/shared/IconApp';
import TextApp from 'components/shared/TextApp';
import {colors} from 'global/colors';
import {fonts} from 'global/fonts';
import React from 'react';
import {StyleSheet, View} from 'react-native';
import {fontSize, normalize} from 'utilities/fontSize';
import Option from './Option';
import PosterMovie from './PosterMovie';
import SingleGenre from './SingleGenre';

const SingleMovie = props => {
  const {genres, poster, title, imdb_rating, runtime, year} = props.item;

  return (
    <View style={styles.item}>
      <PosterMovie poster={poster} />
      <View style={styles.itemContent}>
        <View style={styles.itemHeader}>
          <TextApp style={styles.title} font={fonts.bold} size={fontSize.large}>
            {title}
          </TextApp>
          <View style={styles.star}>
            <IconApp style={styles.starIcon} name="star" />
            <TextApp style={styles.starText}>{imdb_rating}</TextApp>
          </View>
        </View>
        <Option title="Time" content={runtime} />
        <Option title="Year" content={year} />
        <SingleGenre data={genres} />
      </View>
    </View>
  );
};

export default SingleMovie;

const styles = StyleSheet.create({
  item: {
    flexDirection: 'row',
    borderColor: colors.white,
    marginBottom: normalize(20),
  },
  title: {
    flex: 1,
    marginRight: normalize(10),
  },
  itemContent: {
    flex: 3,
  },
  itemHeader: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginBottom: normalize(10),
  },
  star: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  starText: {
    color: colors.yellow,
  },
  starIcon: {
    color: colors.yellow,
    marginRight: normalize(5),
  },
});
