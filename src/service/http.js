import AsyncStorage from '@react-native-async-storage/async-storage';
import Toast from 'react-native-toast-message';
import axios from 'axios';

const HOST = 'https://www.digikala.com';

const PREFIX = 'front-end';
const BASE_URL = `${HOST}/${PREFIX}/`;

const instance = axios.create({
  baseURL: BASE_URL,
  timeout: 30000,
});

const post = async (url, body) => {
  const TOKEN = await AsyncStorage.getItem('token');
  try {
    let response = await instance.post(BASE_URL + url, body, {
      headers: {
        Authorization: `${TOKEN}`,
      },
    });
    if (response.data?.status === 200) {
      return response.data;
    } else {
      Toast.show({
        text1: response.data?.message ?? 'Error Occured',
        type: 'error',
      });
      return Promise.reject(response.data);
    }
  } catch (error) {
    if (error.message === 'Network Error') {
      Toast.show({
        text1: 'Please check your internet connection',
        type: 'warning',
      });
      throw new Error('Please check your internet connection');
    } else {
      Toast.show({
        text1: error?.message ?? 'Error Occured',
        type: 'error',
      });
      return Promise.reject(error.response);
    }
  }
};

const get = async (url, params = '') => {
  const TOKEN = await AsyncStorage.getItem('token');
  try {
    let response = await instance.get(BASE_URL + url, {
      headers: {
        Authorization: `${TOKEN}`,
      },
    });
    if (response.data?.status === 200) {
      return response.data;
    } else {
      Toast.show({
        text1: response?.message ?? 'Error Occured',
        type: 'error',
      });
      return Promise.reject(response.data);
    }
  } catch (error) {
    if (error.message === 'Network Error') {
      Toast.show({
        text1: 'Please check your internet connection',
        type: 'warning',
      });
      throw new Error('Please check your internet connection');
    } else {
      Toast.show({
        text1: error?.message ?? 'Error Occured',
        type: 'error',
      });
      return Promise.reject(error.response);
    }
  }
};

export {post, get};
