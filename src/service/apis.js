import {urls} from 'service/urls';
import {post, get} from 'service/http';

const api = () => ({
  async login(data) {
    const result = await post(urls.LOGIN, data);
    return result;
  },

  async getMovies(title, genre) {
    const result = await get(urls.MOVIE + `?title=${title}` + genre);
    return result;
  },

  async getCategories() {
    const result = await get(urls.CATEGORY + '&page=1');
    return result;
  },
});

export default api;
