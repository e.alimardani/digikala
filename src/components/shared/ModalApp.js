import {colors} from 'global/colors';
import {shadows} from 'global/shadows';
import React from 'react';
import {View, StyleSheet} from 'react-native';
import Modal from 'react-native-modal';
import {normalize} from 'utilities/fontSize';

const ModalApp = props => {
  const {children, isVisible, onClose} = props;
  return (
    <Modal
      statusBarTranslucent
      animationIn="slideInUp"
      animationOut="slideOutDown"
      animationInTiming={400}
      animationOutTiming={400}
      useNativeDriver
      onBackButtonPress={onClose}
      onBackdropPress={onClose}
      isVisible={isVisible}
      backdropOpacity={0.6}
      style={styles.container}>
      <View style={styles.content}>{children}</View>
    </Modal>
  );
};
export default ModalApp;

const styles = StyleSheet.create({
  container: {margin: 0},
  content: {
    backgroundColor: colors.yellow,
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    borderTopRightRadius: 10,
    borderTopLeftRadius: 10,
    ...shadows.ShadowFour,
    padding: normalize(25),
    height: '75%',
  },
});
